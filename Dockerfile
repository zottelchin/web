FROM golang:alpine
RUN apk add --no-cache git upx
COPY go.mod /build/web/
WORKDIR /build/web
RUN go mod download
COPY *.go /build/web/
COPY cmd /build/web/cmd
RUN CGO_ENABLED=0 go build -a -ldflags '-s -w -extldflags "-static"' -o web ./cmd
RUN upx web
RUN mkdir -p /output/var/www /output/bin && cp web /output/bin/

FROM scratch
COPY --from=0 /output/ /
WORKDIR /var/www
EXPOSE 80
ENTRYPOINT ["/bin/web"]
