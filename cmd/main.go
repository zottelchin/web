package main

import (
	"codeberg.org/momar/web"
	"fmt"
	"os"
)

func main() {
	if err := web.Run(); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
